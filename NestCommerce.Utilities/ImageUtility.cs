﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace NestCommerce.Utilities
{
    public static class ImageUtility
    {
        public static Image ConvertByteToImage(byte[] bytes)
        {
            var ms = new MemoryStream(bytes);
            return Image.FromStream(ms);
        }
        public static string GenerateImageNameFromTimestamp()
        {
            DateTime value = DateTime.UtcNow.AddHours(6);
            return value.ToString("yyyyMMddHHmmssffff") + ".jpg";
        }
    }
}
