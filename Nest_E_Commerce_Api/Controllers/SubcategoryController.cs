﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using NestCommerce.Core.Interfaces.Managers;
using NestCommerce.Model.Context;
using NestCommerce.Model.ViewModels;
using NestCommerce.Utilities;
using System.Drawing;

namespace NestCommerce.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class SubcategoryController : ControllerBase
    {
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly ISubcategoryManager _subcategoryManager;
        public SubcategoryController(ISubcategoryManager subcategoryManager, IWebHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
            _subcategoryManager = subcategoryManager;
        }

        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAll()
        {
            try
            {
                var subCategories = _subcategoryManager.GetAll();
                if (!subCategories.Any()) return NotFound();
                subCategories = subCategories.OrderBy(c => c.Name).ToList();
                return Ok(subCategories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAllActive()
        {
            try
            {
                var categories = _subcategoryManager.GetActiveSubCategories();
                if (!categories.Any()) return NotFound();
                categories = categories.OrderBy(c => c.Name).ToList();
                return Ok(categories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("/api/[controller]/[action]/{id}")]
        public IActionResult GetById(long id)
        {
            try
            {
                var subCategory = _subcategoryManager.GetById(id);
                if (subCategory == null) return NotFound();
                return Ok(subCategory);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] SubCategoryVm asubcategoryVm)
        {
            if (ModelState.IsValid)
            {
                bool isFound = _subcategoryManager.DoesNameExist(asubcategoryVm.Name);
                if (isFound)
                {
                    return Conflict();
                }

                SubCategory subCategory = new SubCategory();
                string imageName = ImageUtility.GenerateImageNameFromTimestamp();
                if (asubcategoryVm.ImageByte != null)
                {

                    Image image = ImageUtility.ConvertByteToImage(asubcategoryVm.ImageByte);
                    string fileLocation = Constant.SubCategoryImagePathUrl;
                    var vPath = Path.Combine(_hostEnvironment.ContentRootPath, fileLocation);
                    string path = Path.Combine(vPath, imageName);
                    image.Save(path, ImageFormat.Png);
                    string productils = fileLocation + "/" + imageName;

                    subCategory.SubCategoryImage = productils;
                }
                else
                {
                    subCategory.SubCategoryImage = Constant.NoImagePathUrl;
                }

                subCategory.CreatedBy = asubcategoryVm.CreatedBy;
                subCategory.Name = asubcategoryVm.Name;
                subCategory.CreatedOn = DateTime.UtcNow.AddHours(6);
                subCategory.IsActive = asubcategoryVm.IsActive;
                subCategory.IsDeleted = false;
                bool isSaved = _subcategoryManager.Add(subCategory);
                if (isSaved)
                {
                    return Created(new Uri(Request.GetDisplayUrl()), asubcategoryVm);
                }
            }
            return BadRequest();
        }

        [HttpPost]
        public IActionResult Edit([FromBody] SubCategoryVm aSubcategoryVm)
        {
            var subCategory = _subcategoryManager.GetById(aSubcategoryVm.Id);
            if (subCategory.Name != aSubcategoryVm.Name)
            {
                bool isFound = _subcategoryManager.DoesNameExist(aSubcategoryVm.Name);
                if (isFound)
                {
                    return Conflict();
                }
                try
                {
                    string imageName = ImageUtility.GenerateImageNameFromTimestamp();
                    if (aSubcategoryVm.ImageByte != null)
                    {
                        Image image = ImageUtility.ConvertByteToImage(aSubcategoryVm.ImageByte);
                        if (image != null)
                        {
                            string fileLocation = Constant.SubCategoryImagePathUrl;
                            var vPath = Path.Combine(_hostEnvironment.ContentRootPath, fileLocation);
                            string path = Path.Combine(vPath, imageName);
                            image.Save(path, ImageFormat.Png);
                            string productils = fileLocation + "/" + imageName;
                            subCategory.SubCategoryImage = productils;
                        }
                    }
                    subCategory.Name = aSubcategoryVm.Name;
                    subCategory.ModifiedOn = aSubcategoryVm.ModifiedOn;
                    subCategory.ModifiedBy = aSubcategoryVm.ModifiedBy;
                    subCategory.IsActive = aSubcategoryVm.IsActive;
                    subCategory.IsDeleted = false;
                    bool isSaved = _subcategoryManager.Update(subCategory);
                    if (isSaved)
                    {
                        return Ok();
                    }
                    return NotFound();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            try
            {
                string imageName = ImageUtility.GenerateImageNameFromTimestamp();
                if (aSubcategoryVm.ImageByte != null)
                {
                    Image image = ImageUtility.ConvertByteToImage(aSubcategoryVm.ImageByte);
                    if (image != null)
                    {
                        string fileLocation = Constant.SubCategoryImagePathUrl;
                        var vPath = Path.Combine(_hostEnvironment.ContentRootPath, fileLocation);
                        string path = Path.Combine(vPath, imageName);
                        image.Save(path, ImageFormat.Png);
                        string productils = fileLocation + "/" + imageName;
                        subCategory.SubCategoryImage = productils;
                    }
                }
                subCategory.ModifiedOn = aSubcategoryVm.ModifiedOn;
                subCategory.ModifiedBy = aSubcategoryVm.ModifiedBy;
                subCategory.IsActive = aSubcategoryVm.IsActive;
                subCategory.IsDeleted = false;
                bool isSaved = _subcategoryManager.Update(subCategory);
                if (isSaved)
                {
                    return Ok();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}
