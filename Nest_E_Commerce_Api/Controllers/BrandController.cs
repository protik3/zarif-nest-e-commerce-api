﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using NestCommerce.Core.Interfaces.Managers;
using NestCommerce.Model.Context;
using NestCommerce.Model.ViewModels;
using NestCommerce.Utilities;

namespace NestCommerce.Api.Controllers
{
    
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BrandController : ControllerBase
    {
        private readonly IBrandManager _brandManager;
        private readonly IWebHostEnvironment _hostEnvironment;
        public BrandController(IBrandManager brandManager, IWebHostEnvironment hostEnvironment)
        {
            _brandManager = brandManager;
            _hostEnvironment = hostEnvironment;
        }
        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAll()
        {
            try
            {
                var brands = _brandManager.GetAll();
                if (!brands.Any()) return NotFound();
                brands = brands.OrderBy(c => c.Name).ToList();
                return Ok(brands);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAllActive()
        {
            try
            {
                var brands = _brandManager.GetActiveBrands();
                if (!brands.Any()) return NotFound();
                brands = brands.OrderBy(c => c.Name).ToList();
                return Ok(brands);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("/api/[controller]/[action]/{id}")]
        public IActionResult GetById(long id)
        {
            try
            {
                var brand = _brandManager.GetById(id);
                if (brand == null) return NotFound();
                return Ok(brand);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        public IActionResult Create([FromBody] BrandVm aBrand)
        {
            if (ModelState.IsValid)
            {
                bool isFound = _brandManager.DoesBrandNameExist(aBrand.Name);
                if (isFound)
                {
                    return Conflict();
                }

                Brand brand = new Brand();
                string imageName = ImageUtility.GenerateImageNameFromTimestamp();
                if (aBrand.ImageByte != null)
                {
                  
                    Image image = ImageUtility.ConvertByteToImage(aBrand.ImageByte);
                    string fileLocation = Constant.BrandImagePathUrl;
                    var vPath = Path.Combine(_hostEnvironment.ContentRootPath, fileLocation);
                    string path = Path.Combine(vPath, imageName);
                    image.Save(path, ImageFormat.Png);
                    string productils = fileLocation + "/" + imageName;

                    brand.BrandImage = productils;
                }
                else
                {
                    brand.BrandImage = Constant.NoImagePathUrl;
                }

                brand.CreatedBy = aBrand.CreatedBy;
                brand.Name = aBrand.Name;
                brand.CreatedOn = DateTime.UtcNow.AddHours(6);
                brand.IsActive = aBrand.IsActive;
                brand.IsDeleted = false;
                bool isSaved = _brandManager.Add(brand);
                if (isSaved)
                {
                    return Created(new Uri(Request.GetDisplayUrl()), aBrand);
                }
            }
            return BadRequest();
        }



        [HttpPost]
        public IActionResult Edit([FromBody] BrandVm aBrand)
        {
            var brand = _brandManager.GetById(aBrand.Id);
            if (brand.Name != aBrand.Name)
            {
                bool isFound = _brandManager.DoesBrandNameExist(aBrand.Name);
                if (isFound)
                {
                    return Conflict();
                }
                try
                {
                    string imageName = ImageUtility.GenerateImageNameFromTimestamp();
                    if (aBrand.ImageByte != null)
                    {
                        Image image = ImageUtility.ConvertByteToImage(aBrand.ImageByte);
                        if (image != null)
                        {
                            string fileLocation = Constant.BrandImagePathUrl;
                            var vPath = Path.Combine(_hostEnvironment.ContentRootPath, fileLocation);
                            string path = Path.Combine(vPath, imageName);
                            image.Save(path, ImageFormat.Png);
                            string productils = fileLocation + "/" + imageName;
                            brand.BrandImage = productils;
                        }
                    }
                    brand.Name = aBrand.Name;
                    brand.ModifiedOn = aBrand.ModifiedOn;
                    brand.ModifiedBy = aBrand.ModifiedBy;
                    brand.IsActive = aBrand.IsActive;
                    brand.IsDeleted = false;
                    bool isSaved = _brandManager.Update(brand);
                    if (isSaved)
                    {
                        return Ok();
                    }
                    return NotFound();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            try
            {
                string imageName = ImageUtility.GenerateImageNameFromTimestamp();
                if (aBrand.ImageByte != null)
                {
                    Image image = ImageUtility.ConvertByteToImage(aBrand.ImageByte);
                    if (image != null)
                    {
                        string fileLocation = Constant.BrandImagePathUrl;
                        var vPath = Path.Combine(_hostEnvironment.ContentRootPath, fileLocation);
                        string path = Path.Combine(vPath, imageName);
                        image.Save(path, ImageFormat.Png);
                        string productils = fileLocation + "/" + imageName;
                        brand.BrandImage = productils;
                    }
                }
                brand.ModifiedOn = aBrand.ModifiedOn;
                brand.ModifiedBy = aBrand.ModifiedBy;
                brand.IsActive = aBrand.IsActive;
                brand.IsDeleted = false;
                bool isSaved = _brandManager.Update(brand);
                if (isSaved)
                {
                    return Ok();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }

}
