﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using NestCommerce.Core.Interfaces.Managers;
using NestCommerce.Model.Context;
using NestCommerce.Model.ViewModels;
using NestCommerce.Utilities;


namespace NestCommerce.Api.Controllers
{

    [ApiController]
    [Route("api/[controller]/[action]")]
    public class CategoryController : ControllerBase
    {
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly ICategoryManager _categoryManager;
        public CategoryController(ICategoryManager categoryManager, IWebHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
            _categoryManager = categoryManager;
        }

        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAll()
        {
            try
            {
                var categories = _categoryManager.GetAll();
                if (!categories.Any()) return NotFound();
                categories = categories.OrderBy(c => c.Name).ToList();
                return Ok(categories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAllActive()
        {
            try
            {
                var activeCategories = _categoryManager.GetActiveCategories();
                if (!activeCategories.Any()) return NotFound();
                activeCategories = activeCategories.OrderBy(c => c.Name).ToList();
                return Ok(activeCategories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("/api/[controller]/[action]/{id}")]
        public IActionResult GetById(long id)
        {
            try
            {
                var category = _categoryManager.GetById(id);
                if (category == null) return NotFound();
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] CategoryVm aCategoryVm)
        {
            if (ModelState.IsValid)
            {
                bool isFound = _categoryManager.DoesNameExist(aCategoryVm.Name);
                if (isFound)
                {
                    return Conflict();
                }

                Category category = new Category();
                string imageName = ImageUtility.GenerateImageNameFromTimestamp();
                if (aCategoryVm.ImageByte != null)
                {

                    Image image = ImageUtility.ConvertByteToImage(aCategoryVm.ImageByte);
                    string fileLocation = Constant.CategoryImagePathUrl;
                    var vPath = Path.Combine(_hostEnvironment.ContentRootPath, fileLocation);
                    string path = Path.Combine(vPath, imageName);
                    image.Save(path, ImageFormat.Png);
                    string location = fileLocation + "/" + imageName;

                    category.CategoryImage = location;
                }
                else
                {
                    category.CategoryImage = Constant.NoImagePathUrl;
                }

                category.CreatedBy = aCategoryVm.CreatedBy;
                category.Name = aCategoryVm.Name;
                category.CreatedOn = DateTime.UtcNow.AddHours(6);
                category.IsActive = aCategoryVm.IsActive;
                category.IsDeleted = false;
                bool isSaved = _categoryManager.Add(category);
                if (isSaved)
                {
                    return Created(new Uri(Request.GetDisplayUrl()), aCategoryVm);
                }
            }
            return BadRequest();
        }


        [HttpPost]
        public IActionResult Edit([FromBody] CategoryVm aCategoryVm)
        {
            var category = _categoryManager.GetById(aCategoryVm.Id);
            if (category.Name != aCategoryVm.Name)
            {
                bool isFound = _categoryManager.DoesNameExist(aCategoryVm.Name);
                if (isFound)
                {
                    return Conflict();
                }
                try
                {
                    string imageName = ImageUtility.GenerateImageNameFromTimestamp();
                    if (aCategoryVm.ImageByte != null)
                    {
                        Image image = ImageUtility.ConvertByteToImage(aCategoryVm.ImageByte);
                        if (image != null)
                        {
                            string fileLocation = Constant.CategoryImagePathUrl;
                            var vPath = Path.Combine(_hostEnvironment.ContentRootPath, fileLocation);
                            string path = Path.Combine(vPath, imageName);
                            image.Save(path, ImageFormat.Png);
                            string location = fileLocation + "/" + imageName;
                            category.CategoryImage = location;
                        }
                    }
                    category.Name = aCategoryVm.Name;
                    category.ModifiedOn = aCategoryVm.ModifiedOn;
                    category.ModifiedBy = aCategoryVm.ModifiedBy;
                    category.IsActive = aCategoryVm.IsActive;
                    category.IsDeleted = false;
                    bool isSaved = _categoryManager.Update(category);
                    if (isSaved)
                    {
                        return Ok();
                    }
                    return NotFound();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            try
            {
                string imageName = ImageUtility.GenerateImageNameFromTimestamp();
                if (aCategoryVm.ImageByte != null)
                {
                    Image image = ImageUtility.ConvertByteToImage(aCategoryVm.ImageByte);
                    if (image != null)
                    {
                        string fileLocation = Constant.CategoryImagePathUrl;
                        var vPath = Path.Combine(_hostEnvironment.ContentRootPath, fileLocation);
                        string path = Path.Combine(vPath, imageName);
                        image.Save(path, ImageFormat.Png);
                        string location = fileLocation + "/" + imageName;
                        category.CategoryImage = location;
                    }
                }
                category.ModifiedOn = aCategoryVm.ModifiedOn;
                category.ModifiedBy = aCategoryVm.ModifiedBy;
                category.IsActive = aCategoryVm.IsActive;
                category.IsDeleted = false;
                bool isSaved = _categoryManager.Update(category);
                if (isSaved)
                {
                    return Ok();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
