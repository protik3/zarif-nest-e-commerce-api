﻿using Microsoft.AspNetCore.Mvc;
using NestCommerce.Core.Interfaces.Managers;
using NestCommerce.Model.Context;
using System;
using System.Linq;
using Microsoft.AspNetCore.Http.Extensions;
using System.Collections.Generic;


namespace NestCommerce.Api.Controllers
{
    
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class SearchTagController : ControllerBase
    {
        private readonly ISearchTagManager _searchTagManager;
        public SearchTagController(ISearchTagManager searchTagManager)
        {
            _searchTagManager = searchTagManager;
        }

        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAll()
        {
            try
            {
                ICollection<SearchTag> searchTags = _searchTagManager.GetAll();
                if (!searchTags.Any()) return NotFound();
                var searchTagOrder = searchTags.OrderBy(d => d.Name);
                return Ok(searchTagOrder);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAllActive()
        {
            try
            {
                ICollection<SearchTag> searchTags = _searchTagManager.GetActiveSearchTag();
                if (!searchTags.Any()) return NotFound();
                var searchTagsOrder = searchTags.OrderBy(d => d.Name);
                return Ok(searchTagsOrder);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("/api/[controller]/[action]/{id}")]
        public IActionResult GetById(long id)
        {
            try
            {
                SearchTag searchTag = _searchTagManager.GetById(id);
                if (searchTag == null) return NotFound();
                return Ok(searchTag);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] SearchTag aSearchTag)
        {
            if (ModelState.IsValid)
            {
                bool isFound = _searchTagManager.DoesNameExist(aSearchTag.Name);
                if (isFound)
                {
                    return Conflict();
                }
                aSearchTag.CreatedOn = DateTime.UtcNow.AddHours(6);
                aSearchTag.IsDeleted = false;
                var isSaved = _searchTagManager.Add(aSearchTag);

                if (isSaved)
                {
                    return Created(new Uri(Request.GetDisplayUrl()), aSearchTag);
                }
            }
            return BadRequest();
        }

        [HttpPost]
        public IActionResult Edit([FromBody] SearchTag aSearchTag)
        {
            var searchTag = _searchTagManager.GetById(aSearchTag.Id);
            if (searchTag.Name == aSearchTag.Name)
            {
                try
                {
                    bool isSaved = _searchTagManager.Update(aSearchTag);
                    if (isSaved)
                    {
                        return Ok();
                    }
                    return NotFound();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            bool isFound = _searchTagManager.DoesNameExist(aSearchTag.Name);
            if (isFound)
            {
                return Conflict();
            }
            try
            {
                bool isSaved = _searchTagManager.Update(aSearchTag);
                if (isSaved)
                {
                    return Ok();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
