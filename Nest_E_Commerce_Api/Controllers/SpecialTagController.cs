﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using NestCommerce.Core.Interfaces.Managers;
using NestCommerce.Model.Context;

namespace NestCommerce.Api.Controllers
{
    
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class SpecialTagController : ControllerBase
    {
        private readonly ISpecialTagManager _specialTagManager;
        public SpecialTagController(ISpecialTagManager specialTagManager)
        {
            _specialTagManager = specialTagManager;
        }


        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAll()
        {
            try
            {
                ICollection<SpecialTag> specialTags = _specialTagManager.GetAll();
                if (!specialTags.Any()) return NotFound();
                var tagOrder = specialTags.OrderBy(d => d.Name);
                return Ok(tagOrder);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAllActive()
        {
            try
            {
                ICollection<SpecialTag> activeSpecialTag = _specialTagManager.GetActiveSpecialTag();
                if (!activeSpecialTag.Any()) return NotFound();
                var tagsOrder = activeSpecialTag.OrderBy(d => d.Name);
                return Ok(tagsOrder);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("/api/[controller]/[action]/{id}")]
        public IActionResult GetById(long id)
        {
            try
            {
                SpecialTag tag = _specialTagManager.GetById(id);
                if (tag == null) return NotFound();
                return Ok(tag);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpPost]
        public IActionResult Create([FromBody] SpecialTag tag)
        {
            if (ModelState.IsValid)
            {
                bool isFound = _specialTagManager.DoesNameExist(tag.Name);
                if (isFound)
                {
                    return Conflict();
                }
                tag.CreatedOn = DateTime.UtcNow.AddHours(6);
                tag.IsDeleted = false;
                var isSaved = _specialTagManager.Add(tag);

                if (isSaved)
                {
                    return Created(new Uri(Request.GetDisplayUrl()), tag);
                }
            }
            return BadRequest();
        }

        [HttpPost]
        public IActionResult Edit([FromBody] SpecialTag tag)
        {
            var searchTag = _specialTagManager.GetById(tag.Id);
            if (searchTag.Name == tag.Name)
            {
                try
                {
                    bool isSaved = _specialTagManager.Update(tag);
                    if (isSaved)
                    {
                        return Ok();
                    }
                    return NotFound();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            bool isFound = _specialTagManager.DoesNameExist(tag.Name);
            if (isFound)
            {
                return Conflict();
            }
            try
            {
                bool isSaved = _specialTagManager.Update(tag);
                if (isSaved)
                {
                    return Ok();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
