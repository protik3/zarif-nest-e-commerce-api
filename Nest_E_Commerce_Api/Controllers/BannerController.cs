﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using NestCommerce.Core.Interfaces.Managers;
using NestCommerce.Model.Context;
using NestCommerce.Model.ViewModels;
using NestCommerce.Utilities;

namespace NestCommerce.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BannerController : ControllerBase
    {
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IBannerManager _bannerManager;
        public BannerController(IBannerManager bannerManager,IWebHostEnvironment webHostEnvironment)
        {
            _bannerManager = bannerManager;
            _hostEnvironment = webHostEnvironment;
        }

        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAll()
        {
            try
            {
                var banners = _bannerManager.GetAll();
                if (!banners.Any()) return NotFound();
                return Ok(banners);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("/api/[controller]/[action]")]
        public IActionResult GetAllActive()
        {
            try
            {
                var activeBanner = _bannerManager.GetActiveBanner();
                if (!activeBanner.Any()) return NotFound();
                return Ok(activeBanner);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("/api/[controller]/[action]/{id}")]
        public IActionResult GetById(long id)
        {
            try
            {
                var banner = _bannerManager.GetById(id);
                if (banner == null) return NotFound();
                return Ok(banner);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        public IActionResult Create([FromBody] BannerVm aBannerVm)
        {
            if (ModelState.IsValid)
            {
                Banner banner = new Banner();
                string imageName = ImageUtility.GenerateImageNameFromTimestamp();
                if (aBannerVm.ImageByte != null)
                {

                    Image image = ImageUtility.ConvertByteToImage(aBannerVm.ImageByte);
                    string fileLocation = Constant.BannerImagePathUrl;
                    var vPath = Path.Combine(_hostEnvironment.ContentRootPath, fileLocation);
                    string path = Path.Combine(vPath, imageName);
                    image.Save(path, ImageFormat.Png);
                    string bannerImg = fileLocation + "/" + imageName;

                    banner.BannerImage = bannerImg;
                }
                else
                {
                    banner.BannerImage = Constant.NoImagePathUrl;
                }

                banner.CreatedBy = aBannerVm.CreatedBy;
                banner.BannerTypeId = aBannerVm.BannerTypeId;
                banner.CreatedOn = DateTime.UtcNow.AddHours(6);
                banner.IsActive = aBannerVm.IsActive;
                banner.IsDelete = false;
                bool isSaved = _bannerManager.Add(banner);
                if (isSaved)
                {
                    return Created(new Uri(Request.GetDisplayUrl()), aBannerVm);
                }
            }
            return BadRequest();
        }


        [HttpPost]
        public IActionResult Edit([FromBody] BannerVm aBannerVm)
        {
            var banner = _bannerManager.GetById(aBannerVm.Id);
            try
            {
                string imageName = ImageUtility.GenerateImageNameFromTimestamp();
                if (aBannerVm.ImageByte != null)
                {
                    Image image = ImageUtility.ConvertByteToImage(aBannerVm.ImageByte);
                    if (image != null)
                    {
                        string fileLocation = Constant.BannerImagePathUrl;
                        var vPath = Path.Combine(_hostEnvironment.ContentRootPath, fileLocation);
                        string path = Path.Combine(vPath, imageName);
                        image.Save(path, ImageFormat.Png);
                        string location = fileLocation + "/" + imageName;
                        banner.BannerImage = location;
                    }
                }
                banner.ModifiedOn = aBannerVm.ModifiedOn;
                banner.BannerTypeId = aBannerVm.BannerTypeId;
                banner.ModifiedBy = aBannerVm.ModifiedBy;
                banner.IsActive = aBannerVm.IsActive;
                banner.IsDelete = false;
                bool isSaved = _bannerManager.Update(banner);
                if (isSaved)
                {
                    return Ok();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
