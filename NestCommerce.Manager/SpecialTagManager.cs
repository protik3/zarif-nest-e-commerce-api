﻿using NestCommerce.Core.Interfaces.Managers;
using NestCommerce.Model.Context;
using NestCommerce.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace NestCommerce.Manager
{
    public class SpecialTagManager : CommonManager<SpecialTag>, ISpecialTagManager
    {
        public SpecialTagManager() : base(new SpecialTagRepository())
        {
        }

        public SpecialTag GetById(long id)
        {
            return GetFirstOrDefault(c => c.Id == id
                && !c.IsDeleted);
        }

        public bool DoesNameExist(string name)
        {
            SpecialTag specialTag = GetFirstOrDefault(c => c.Name.ToLower().Equals(name.ToLower())
                && !c.IsDeleted);
            return specialTag != null;
        }

        public ICollection<SpecialTag> GetAll()
        {
            return Get(c => !c.IsDeleted);
        }

        public ICollection<SpecialTag> GetActiveSpecialTag()
        {
            return Get(c => !c.IsDeleted && c.IsActive);
        }

        public int CountTotalSpecialTag()
        {
            var total = Get(c => !c.IsDeleted
                && c.IsActive).Count;
            return total;
        }
    }
}
