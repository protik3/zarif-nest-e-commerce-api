﻿using Microsoft.Extensions.DependencyInjection;
using NestCommerce.Core.Interfaces.Managers;

namespace NestCommerce.Manager
{
    public static class RegisterStartupManagers
    {
        public static void RegisterManagers(this IServiceCollection services)
        {
            services.AddScoped<IBrandManager, BrandManager>();
            services.AddScoped<ISearchTagManager, SearchTagManager>();
            services.AddScoped<ISpecialTagManager, SpecialTagManager>();
            services.AddScoped<ICategoryManager, CategoryManager>();
            services.AddScoped<IBannerManager, BannerManager>();
        }
    }
}
