﻿using System.Collections.Generic;
using NestCommerce.Core.Interfaces.Managers;
using NestCommerce.Model.Context;
using NestCommerce.Repository;

namespace NestCommerce.Manager
{
    public class SubcategoryManager : CommonManager<SubCategory>, ISubcategoryManager
    {
        public SubcategoryManager() : base(new SubcategoryRepository())
        {
        }

        public SubCategory GetById(long id)
        {
            return GetFirstOrDefault(c => c.Id == id
                                          && !c.IsDeleted);
        }

        public bool DoesNameExist(string name)
        {
            SubCategory subcategory = GetFirstOrDefault(c => c.Name.ToLower().Equals(name.ToLower())
                                                          && !c.IsDeleted);
            return subcategory != null;
        }

        public ICollection<SubCategory> GetAll()
        {
            return Get(c => !c.IsDeleted);
        }

        public ICollection<SubCategory> GetActiveSubCategories()
        {
            return Get(c => !c.IsDeleted && c.IsActive);
        }

        public int CountTotalSubCategory()
        {
            var total = Get(c => !c.IsDeleted
                                 && c.IsActive).Count;
            return total;
        }
    }
}