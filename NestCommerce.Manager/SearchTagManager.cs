﻿using NestCommerce.Core.Interfaces.Managers;
using NestCommerce.Model.Context;
using NestCommerce.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace NestCommerce.Manager
{
    public class SearchTagManager : CommonManager<SearchTag>, ISearchTagManager
    {
        public SearchTagManager() : base(new SearchTagRepository())
        {
        }

        public SearchTag GetById(long id)
        {
            return GetFirstOrDefault(c => c.Id == id
                && !c.IsDeleted);
        }

        public bool DoesNameExist(string name)
        {
            SearchTag searchTag = GetFirstOrDefault(c => c.Name.ToLower().Equals(name.ToLower())
                && !c.IsDeleted);
            return searchTag != null;
        }

        public ICollection<SearchTag> GetAll()
        {
            return Get(c => !c.IsDeleted);
        }

        public ICollection<SearchTag> GetActiveSearchTag()
        {
            return Get(c => !c.IsDeleted && c.IsActive);
        }

        public int CountTotalSearchTag()
        {
            var total = Get(c => !c.IsDeleted
                && c.IsActive).Count;
            return total;
        }
    }
}
