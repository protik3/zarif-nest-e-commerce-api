﻿using System;
using System.Collections.Generic;
using System.Text;
using NestCommerce.Core.Interfaces.Managers;
using NestCommerce.Model.Context;
using NestCommerce.Repository;

namespace NestCommerce.Manager
{
    public class CategoryManager : CommonManager<Category>, ICategoryManager
    {
        public CategoryManager() : base(new CategoryRepository())
        {
        }

        public Category GetById(long id)
        {
            return GetFirstOrDefault(c => c.Id == id
                                          && !c.IsDeleted);
        }

        public bool DoesNameExist(string name)
        {
            Category category = GetFirstOrDefault(c => c.Name.ToLower().Equals(name.ToLower())
                                                    && !c.IsDeleted);
            return category != null;
        }

        public ICollection<Category> GetAll()
        {
            return Get(c => !c.IsDeleted);
        }

        public ICollection<Category> GetActiveCategories()
        {
            return Get(c => !c.IsDeleted && c.IsActive);
        }

        public int CountTotalCategory()
        {
            var total = Get(c => !c.IsDeleted
                                      && c.IsActive).Count;
            return total;
        }
    }
}
