﻿using System.Collections.Generic;
using NestCommerce.Core.Interfaces.Managers;
using NestCommerce.Model.Context;
using NestCommerce.Repository;

namespace NestCommerce.Manager
{
    public class BannerManager : CommonManager<Banner>, IBannerManager
    {
        public BannerManager() : base(new BannerRepository())
        {
        }

        public Banner GetById(long id)
        {
            return GetFirstOrDefault(c => c.Id == id
                                          && !c.IsDelete,x=>x.BannerType);
        }

        public ICollection<Banner> GetAll()
        {
            return Get(c => !c.IsDelete,x=>x.BannerType);
        }

        public ICollection<Banner> GetActiveBanner()
        {
            return Get(c => !c.IsDelete && c.IsActive,x=>x.BannerType);
        }
    }
}