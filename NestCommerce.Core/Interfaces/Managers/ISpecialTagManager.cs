﻿using NestCommerce.Model.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace NestCommerce.Core.Interfaces.Managers
{
    public interface ISpecialTagManager : ICommonManager<SpecialTag>
    {
        SpecialTag GetById(long id);
        bool DoesNameExist(string name);
        ICollection<SpecialTag> GetAll();
        ICollection<SpecialTag> GetActiveSpecialTag();
        int CountTotalSpecialTag();
    }
}
