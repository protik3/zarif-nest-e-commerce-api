﻿using System;
using System.Collections.Generic;
using System.Text;
using NestCommerce.Model.Context;

namespace NestCommerce.Core.Interfaces.Managers
{
    public interface ICategoryManager : ICommonManager<Category>
    {
        Category GetById(long id);
        bool DoesNameExist(string name);
        ICollection<Category> GetAll();
        ICollection<Category> GetActiveCategories();
        int CountTotalCategory();
    }
}
