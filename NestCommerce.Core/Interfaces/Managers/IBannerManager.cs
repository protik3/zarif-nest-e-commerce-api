﻿using System.Collections.Generic;
using NestCommerce.Model.Context;

namespace NestCommerce.Core.Interfaces.Managers
{
    public interface IBannerManager : ICommonManager<Banner>
    {
        Banner GetById(long id);
        ICollection<Banner> GetAll();
        ICollection<Banner> GetActiveBanner();

    }
}