﻿using NestCommerce.Model.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace NestCommerce.Core.Interfaces.Managers
{
    public interface ISearchTagManager : ICommonManager<SearchTag>
    {
        SearchTag GetById(long id);
        bool DoesNameExist(string name);
        ICollection<SearchTag> GetAll();
        ICollection<SearchTag> GetActiveSearchTag();
        int CountTotalSearchTag();
    }
}
