﻿using NestCommerce.Model.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace NestCommerce.Core.Interfaces.Managers
{
    public interface IBrandManager : ICommonManager<Brand>
    {
        Brand GetById(long brandId);
        bool DoesBrandNameExist(string name);
        ICollection<Brand> GetAll();
        ICollection<Brand> GetActiveBrands();
        int CountTotalBrand();
    }
}
