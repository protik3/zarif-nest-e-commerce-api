﻿using System.Collections.Generic;
using NestCommerce.Model.Context;

namespace NestCommerce.Core.Interfaces.Managers
{
    public interface ISubcategoryManager : ICommonManager<SubCategory>
    {
        SubCategory GetById(long id);
        bool DoesNameExist(string name);
        ICollection<SubCategory> GetAll();
        ICollection<SubCategory> GetActiveSubCategories();
        int CountTotalSubCategory();
    }
}