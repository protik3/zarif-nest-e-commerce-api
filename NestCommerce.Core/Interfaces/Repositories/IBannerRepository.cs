﻿using NestCommerce.Model.Context;

namespace NestCommerce.Core.Interfaces.Repositories
{
    public interface IBannerRepository : ICommonRepository<Banner>
    {
        
    }
}