﻿using NestCommerce.Model.Context;

namespace NestCommerce.Core.Interfaces.Repositories
{
    public interface ISubcategoryRepository : ICommonRepository<SubCategory>
    {
    }
}