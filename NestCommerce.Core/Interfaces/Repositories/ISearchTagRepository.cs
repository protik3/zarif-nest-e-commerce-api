﻿using NestCommerce.Model.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace NestCommerce.Core.Interfaces.Repositories
{
    public interface ISearchTagRepository : ICommonRepository<SearchTag>
    {
    }
}
