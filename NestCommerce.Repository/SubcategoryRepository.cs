﻿using System;
using System.Collections.Generic;
using System.Text;
using NestCommerce.Core.Interfaces.Repositories;
using NestCommerce.Model.Context;

namespace NestCommerce.Repository
{
    public class SubcategoryRepository : CommonRepository<SubCategory>, ISubcategoryRepository
    {
        public SubcategoryRepository() : base(new ZarifNestDBContext())
        {
        }
    }
}
