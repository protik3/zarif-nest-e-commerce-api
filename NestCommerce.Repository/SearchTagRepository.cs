﻿using NestCommerce.Core.Interfaces.Repositories;
using NestCommerce.Model.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace NestCommerce.Repository
{
    public class SearchTagRepository : CommonRepository<SearchTag>, ISearchTagRepository
    {
        public SearchTagRepository() : base(new ZarifNestDBContext())
        {
        }
    }
}
