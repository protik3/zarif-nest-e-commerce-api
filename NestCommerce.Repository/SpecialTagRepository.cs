﻿using NestCommerce.Core.Interfaces.Repositories;
using NestCommerce.Model.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace NestCommerce.Repository
{
    public class SpecialTagRepository : CommonRepository<SpecialTag>, ISpecialTagRepository
    {
        public SpecialTagRepository() : base(new ZarifNestDBContext())
        {
        }
    }
}
