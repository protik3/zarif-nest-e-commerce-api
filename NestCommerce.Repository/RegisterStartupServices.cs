﻿using Microsoft.Extensions.DependencyInjection;
using NestCommerce.Core.Interfaces.Repositories;


namespace NestCommerce.Repository
{
    public static class RegisterStartupServices
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IBrandRepository, BrandRepository>();
            services.AddScoped<ISearchTagRepository, SearchTagRepository>();
            services.AddScoped<ISpecialTagRepository, SpecialTagRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<ISubcategoryRepository, SubcategoryRepository>();
            services.AddScoped<IBannerRepository, BannerRepository>();
        }
    }
}
