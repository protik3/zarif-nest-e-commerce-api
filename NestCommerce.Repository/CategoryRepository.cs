﻿using System;
using System.Collections.Generic;
using System.Text;
using NestCommerce.Core.Interfaces.Repositories;
using NestCommerce.Model.Context;

namespace NestCommerce.Repository
{
    public class CategoryRepository : CommonRepository<Category>, ICategoryRepository
    {
        public CategoryRepository() : base(new ZarifNestDBContext())
        {
        }
    }
}
