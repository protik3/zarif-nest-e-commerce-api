﻿using NestCommerce.Core.Interfaces.Repositories;
using NestCommerce.Model.Context;

namespace NestCommerce.Repository
{
    public class BrandRepository : CommonRepository<Brand>, IBrandRepository
    {
        public BrandRepository() : base(new ZarifNestDBContext())
        {
        }
    }
}
