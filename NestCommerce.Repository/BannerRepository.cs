﻿using NestCommerce.Core.Interfaces.Repositories;
using NestCommerce.Model.Context;

namespace NestCommerce.Repository
{
    public class BannerRepository : CommonRepository<Banner>, IBannerRepository
    {
        public BannerRepository() : base(new ZarifNestDBContext())
        {
        }
    }
}