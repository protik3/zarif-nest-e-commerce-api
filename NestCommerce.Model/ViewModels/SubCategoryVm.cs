﻿using System;

namespace NestCommerce.Model.ViewModels
{
    public partial class SubCategoryVm
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string SubCategoryImage { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public byte[] ImageByte { get; set; }
    }
}