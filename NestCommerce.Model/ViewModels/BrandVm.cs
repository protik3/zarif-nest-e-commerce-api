﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;


namespace NestCommerce.Model.ViewModels
{
    public partial class BrandVm
    {
        
        public long Id { get; set; }
        public string Name { get; set; }
        public IFormFile ImageLocation { get; set; }
        public string BrandImage { get; set; }
        public byte[] ImageByte { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }

       
    }
}
