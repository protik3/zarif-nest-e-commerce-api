﻿using System;
using NestCommerce.Model.Context;

namespace NestCommerce.Model.ViewModels
{
    public partial class BannerVm
    {
        public long Id { get; set; }
        public string BannerImage { get; set; }
        public long BannerTypeId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual BannerType BannerType { get; set; }

        public byte[] ImageByte { get; set; }

    }
}