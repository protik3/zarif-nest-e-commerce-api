﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NestCommerce.Model.Context
{
    public partial class OrderReject
    {
        public long Id { get; set; }
        public string Status { get; set; }
    }
}
