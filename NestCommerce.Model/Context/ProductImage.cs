﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NestCommerce.Model.Context
{
    public partial class ProductImage
    {
        public long Id { get; set; }
        public string ImageLocation { get; set; }
        public long ProductId { get; set; }
        public bool IsDeleted { get; set; }

        public virtual Product Product { get; set; }
    }
}
