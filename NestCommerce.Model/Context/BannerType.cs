﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NestCommerce.Model.Context
{
    public partial class BannerType
    {
        public BannerType()
        {
            Banners = new HashSet<Banner>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Banner> Banners { get; set; }
    }
}
