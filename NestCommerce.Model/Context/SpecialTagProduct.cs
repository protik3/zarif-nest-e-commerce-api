﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NestCommerce.Model.Context
{
    public partial class SpecialTagProduct
    {
        public long Id { get; set; }
        public long SpecialTagId { get; set; }
        public long ProductId { get; set; }

        public virtual Product Product { get; set; }
        public virtual SpecialTag SpecialTag { get; set; }
    }
}
