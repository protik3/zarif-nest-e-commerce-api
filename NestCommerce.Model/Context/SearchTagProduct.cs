﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NestCommerce.Model.Context
{
    public partial class SearchTagProduct
    {
        public long Id { get; set; }
        public long SearchTagId { get; set; }
        public long ProductId { get; set; }

        public virtual Product Product { get; set; }
        public virtual SearchTag SearchTag { get; set; }
    }
}
