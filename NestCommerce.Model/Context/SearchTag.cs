﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NestCommerce.Model.Context
{
    public partial class SearchTag
    {
        public SearchTag()
        {
            SearchTagProducts = new HashSet<SearchTagProduct>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual ICollection<SearchTagProduct> SearchTagProducts { get; set; }
    }
}
