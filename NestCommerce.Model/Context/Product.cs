﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NestCommerce.Model.Context
{
    public partial class Product
    {
        public Product()
        {
            ProductDiscounts = new HashSet<ProductDiscount>();
            ProductImages = new HashSet<ProductImage>();
            SearchTagProducts = new HashSet<SearchTagProduct>();
            SpecialTagProducts = new HashSet<SpecialTagProduct>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long CategoryId { get; set; }
        public long BrandId { get; set; }
        public decimal UnitPrice { get; set; }
        public long AvailableStock { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Brand Brand { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<ProductDiscount> ProductDiscounts { get; set; }
        public virtual ICollection<ProductImage> ProductImages { get; set; }
        public virtual ICollection<SearchTagProduct> SearchTagProducts { get; set; }
        public virtual ICollection<SpecialTagProduct> SpecialTagProducts { get; set; }
    }
}
